name := "spark-streaming-boilerplate"

version := "1.0"

/* Use same scala and spark versions as on the remote machine the job will run*/

scalaVersion := "2.11.8"

resolvers ++= Seq(  Resolver sonatypeRepo "public",
                    Resolver typesafeRepo "releases",
                    "MavenRepository" at "https://mvnrepository.com/")

libraryDependencies ++= Seq(
                            "org.apache.spark" % "spark-core_2.11" % "2.2.0",
                            "org.apache.spark" % "spark-sql_2.11" % "2.2.0",
                            "org.apache.spark" %% "spark-streaming" % "2.2.0" % "provided",
                            "com.amazonaws" % "aws-java-sdk" % "1.11.52",
                            "joda-time" % "joda-time" % "2.9.3"
                            )
