# Apache Spark Boilerplate

This repository aims to be an entry point for Data Engineers to quickly put together an Apache Spark Application utilising either SBT or Maven.

## Features

Boilerplate code for:

-  Simple Spark Application
-  Simple Spark Streaming Application

## Getting Started

The instructions below will step you through the process of cloning the projects to your local machine, compiling and packaging sample code to produce a ```.jar``` file and submitting it to an Apache Spark cluster for execution.

### Goals

- Compile/Debug Spark/Scala code.
- Package compiled code into a ```.jar```.
- Deploy '''.jar''' file to execute on a cluster.


### Prerequisites

The following dependencies much be installed as a minimum to compile, package and test Apache Spark applications.

- Java 8+
- Apache Spark 2.2+ (incl all dependencies)
    - You must at minimum have an environment which is able to run a spark-shell
- Build Tool
    - Maven 3.3.9+ or SBT
- Internet access to source dependencies required by the projects.

### Cloning and Compilation

- ```git clone {repository path}```

### CLI Compilation and Packaging

- To begin first navigate to the top level of either a SBT or MVN project.

- Execute the build tool commands corresponding to the project you've chosen.

    - MVN
        - Compile the project

            ```mvn compile```

        - Package the projet

            ```mvn package```

    - SBT 
        - Compile the project

            ```sbt compile```
        - Package the projet

            ```sbt package```
